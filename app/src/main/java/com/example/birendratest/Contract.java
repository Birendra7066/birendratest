package com.example.birendratest;

import android.location.Address;

import com.example.birendratest.model.WeatherData;

import java.util.ArrayList;

public interface Contract {


    interface Presenter{

        void onDestroy();

        void getWeatherAndLocation();

    }

    interface MainView {

        void showProgress();

        void hideProgress();

        void onResponseFailure(Throwable throwable);


        void setDataTorecyclerView(ArrayList<Address> areaList, String city, String temperature, String humidity);

        void notifyAdaterDataSetChanged();
    }


    interface GetUserDataIntractor {

        interface OnFinishedListener {
            void onFinished(WeatherData weatherData);
            void onFailure(Throwable t);
        }

        void getWeatherData(OnFinishedListener onFinishedListener, String city, String mode, String unit, String appId);
    }

}
