package com.example.birendratest;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.birendratest.adapter.AdapterMain;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements Contract.MainView{

    private ProgressBar mProgressBar;
    private Contract.Presenter mPresenter;
    private RecyclerView mRvAreas;
    private RecyclerView.LayoutManager mLayoutManager;
    private AdapterMain mAdapterAreas;
    private Activity mActivity;
    private TextView mLblCity, mLblArea, mLblTemperature, mLblHumidity;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };
    private static final int REQUEST_CODE_INTENT_PERMISSION = 7;
    private static final int REQUEST_PERMISSION_LOCATION = 5;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mActivity = this;

        initViews();
        initProgressBar();

        mPresenter = new PresenterImplementation(mActivity, this, new WeatherServiceImplementation());
        if (checkAndRequestPermissions()){
            mPresenter.getWeatherAndLocation();
        }

    }

    private void initViews(){
        mRvAreas = (RecyclerView) findViewById(R.id.recycler_view_main);
        mLayoutManager = new LinearLayoutManager(mActivity);
        mRvAreas.setLayoutManager(mLayoutManager);
        mRvAreas.setHasFixedSize(true);

        mLblCity = findViewById(R.id.lbl_main_city);
        mLblArea = findViewById(R.id.lbl_main_area);
        mLblTemperature = findViewById(R.id.lbl_main_temperature);
        mLblHumidity = findViewById(R.id.lbl_main_humidity);
    }

    private void initProgressBar() {
        mProgressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        mProgressBar.setIndeterminate(true);

        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(mProgressBar);

        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        mProgressBar.setVisibility(View.INVISIBLE);

        this.addContentView(relativeLayout, params);
    }


    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponseFailure(Throwable throwable) {

        Toast.makeText(mActivity,
                getString(R.string.txt_error_network) + throwable.getMessage(),
                Toast.LENGTH_LONG).show();

    }



    @Override
    public void setDataTorecyclerView(ArrayList<Address> areaList, String city, String temperature, String humidity) {
        mAdapterAreas = new AdapterMain(mActivity, areaList);
        mRvAreas.setAdapter(mAdapterAreas);

        mLblCity.setText(city);
        mLblArea.setText(areaList.get(0).getFeatureName());
        mLblTemperature.setText(temperature );
        mLblHumidity.setText(humidity);

    }

    @Override
    public void notifyAdaterDataSetChanged() {
        mAdapterAreas.notifyDataSetChanged();
    }


    private  boolean checkAndRequestPermissions() {

        int locationPermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionCoarseLocation = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        //Permissions Granted
                        mPresenter.getWeatherAndLocation();

                    } else {
                        //Some Permissions Denied
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK(getString(R.string.txt_required_location_permission),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }

                        else {

                            Toast.makeText(mActivity, getString(R.string.txt_enable_settings), Toast.LENGTH_LONG)
                                    .show();

                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
                            intent.setData(uri);
                            mActivity.startActivityForResult(intent, REQUEST_CODE_INTENT_PERMISSION);
                        }
                    }
                }
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_INTENT_PERMISSION) {

            if (checkAndRequestPermissions()){
                mPresenter.getWeatherAndLocation();
            }

        }

        if (requestCode == REQUEST_PERMISSION_LOCATION){
            if (checkAndRequestPermissions()){
                mPresenter.getWeatherAndLocation();
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.txt_btn_ok, okListener)
                .setNegativeButton(R.string.txt_btn_cancel, okListener)
                .create()
                .show();
    }

}
