package com.example.birendratest;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.example.birendratest.location.AppLocationService;
import com.example.birendratest.location.AreaCallbackListener;
import com.example.birendratest.location.LocationDetails;
import com.example.birendratest.model.WeatherData;
import java.util.ArrayList;

public class PresenterImplementation implements Contract.Presenter, Contract.GetUserDataIntractor.OnFinishedListener, AreaCallbackListener {

    private Contract.MainView mMainView;
    private Contract.GetUserDataIntractor mGetUserDataIntractor;
    private Activity mActivity;
    private ArrayList<Address> mAreaList;
    private String mCity, mTemperature, mHumidity;
    private static final int REQUEST_PERMISSION_LOCATION = 5;


    public PresenterImplementation(Activity mActivity, Contract.MainView mMainView, Contract.GetUserDataIntractor mGetUserDataIntractor) {
        this.mMainView = mMainView;
        this.mGetUserDataIntractor = mGetUserDataIntractor;
        this.mActivity = mActivity;
        mAreaList = new ArrayList<>();
    }

    @Override
    public void onDestroy() {
        mMainView = null;
    }


    @Override
    public void onFinished(WeatherData weatherData) {
        if(mMainView != null){

            mTemperature = weatherData.getMain().getTemp() + " \u2103";
            mHumidity = weatherData.getMain().getHumidity() + "%";
            mMainView.setDataTorecyclerView(mAreaList, mCity, mTemperature,mHumidity);
            mMainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(mMainView != null){
            mMainView.onResponseFailure(t);
            mMainView.hideProgress();
        }
    }

    @Override
    public void getWeatherAndLocation(){

        mMainView.showProgress();

        AppLocationService mAppLocationService = new AppLocationService(
                mActivity);

        Location location = mAppLocationService
                .getLocation(LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER);

        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            LocationDetails.getAddressFromLocation(latitude, longitude,
                    mActivity.getApplicationContext(),this);

        } else {
            showSettingsAlert();
        }

    }

    @Override
    public void onAreaCallBack(ArrayList<Address> listAreas) {
        mAreaList.clear();
        mAreaList.addAll(listAreas);
        mCity = listAreas.get(0).getLocality();

        mGetUserDataIntractor.getWeatherData(this, mCity,
                mActivity.getString(R.string.api_data_format_mode),
                mActivity.getString(R.string.api_unit_metric),
                mActivity.getString(R.string.appId_weather));
    }


    private void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                mActivity);
        alertDialog.setTitle(R.string.txt_setting_title);
        alertDialog.setMessage(R.string.msg_enable_location_provider);
        alertDialog.setPositiveButton(R.string.txt_btn_settings,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mActivity.startActivityForResult(intent, REQUEST_PERMISSION_LOCATION);
                    }
                });
        alertDialog.setNegativeButton(R.string.txt_btn_cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }



}
