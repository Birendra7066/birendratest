package com.example.birendratest;

import android.util.Log;

import com.example.birendratest.model.WeatherData;
import com.example.birendratest.network.RetrofitInstance;
import com.example.birendratest.network.WeatherService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherServiceImplementation implements Contract.GetUserDataIntractor {

    @Override
    public void getWeatherData(final OnFinishedListener onFinishedListener, String city, String mode, String unit, String appId) {

        WeatherService mWeatherService = RetrofitInstance.getRetrofitInsatnace().create(WeatherService.class);

        Call<WeatherData> call = mWeatherService.getWeatherData(city, mode, unit, appId);

        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<WeatherData>() {

            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                onFinishedListener.onFinished(response.body());
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });


    }
}
