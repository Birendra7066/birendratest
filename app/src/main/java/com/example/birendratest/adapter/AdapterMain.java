package com.example.birendratest.adapter;

import android.content.Context;

import android.location.Address;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.birendratest.R;

import java.util.List;


public class AdapterMain extends RecyclerView.Adapter {

    private Context context;
    private List<Address> listAreas;
    private LayoutInflater layoutInflater;

    public AdapterMain(Context context, List<Address> listAreas) {

        this.context = context;
        this.listAreas = listAreas;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View view = inflater.inflate(R.layout.item_areas_main, viewGroup, false);

        viewHolder = new ItemMainViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        ItemMainViewHolder itemEventHome = (ItemMainViewHolder) viewHolder;

            itemEventHome.mLblArea.setText(listAreas.get(position).getFeatureName());

    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {

        if (listAreas != null) {
            return listAreas.size();
        }
        return 0;
    }

    protected class ItemMainViewHolder extends RecyclerView.ViewHolder {

        TextView mLblArea;


        public ItemMainViewHolder(View itemView) {
            super(itemView);

            mLblArea = itemView.findViewById(R.id.lbl_item_main_area);

        }
    }

}

