package com.example.birendratest.location;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.example.birendratest.R;

public class AppLocationService extends Service implements LocationListener {


    protected LocationManager locationManager;
    private Location location;

    private static final long MIN_DISTANCE_FOR_UPDATE = 10;
    private static final long MIN_TIME_FOR_UPDATE = 1000 * 60 * 2;

    private Context context;

    public AppLocationService(Context context) {
        this.context = context;

        locationManager = (LocationManager) context
                .getSystemService(LOCATION_SERVICE);


    }

    public Location getLocation(String gpsProvider, String networkProvider) {
        if (locationManager.isProviderEnabled(gpsProvider) || locationManager.isProviderEnabled(networkProvider)) {

            if (ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(context, getString(R.string.txt_enable_settings), Toast.LENGTH_LONG)
                        .show();

                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context.startActivity(intent);
            }
            else {

                locationManager.requestLocationUpdates(gpsProvider,
                        MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);
                if (locationManager != null) {

                    location = locationManager.getLastKnownLocation(gpsProvider);

                    if (location == null){
                        location = locationManager.getLastKnownLocation(networkProvider);
                    }

                    return location;
                }

            }


        }
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


}
