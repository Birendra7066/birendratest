package com.example.birendratest.location;

import android.location.Address;

import java.util.ArrayList;

public interface AreaCallbackListener {

    void onAreaCallBack(ArrayList<Address> listAreas);

}
