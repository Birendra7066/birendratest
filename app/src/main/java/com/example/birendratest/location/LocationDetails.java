package com.example.birendratest.location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.example.birendratest.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LocationDetails {
    private static final String TAG = "LocationDetails";


    public static void getAddressFromLocation(final double latitude, final double longitude,
                                              final Context context, final AreaCallbackListener areaListener) {

        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                List<Address> addressList = new ArrayList<>();

                try {
                    addressList.addAll(geocoder.getFromLocation(
                            latitude, longitude, 10));

                } catch (IOException e) {
                    Log.e(TAG, context.getResources().getString(R.string.msg_error_connect_geocoder), e);
                } finally {
                    areaListener.onAreaCallBack((ArrayList<Address>) addressList);
                }
            }
        };
        thread.start();
    }
}
