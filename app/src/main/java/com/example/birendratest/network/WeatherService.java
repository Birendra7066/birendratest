package com.example.birendratest.network;


import com.example.birendratest.model.WeatherData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("weather")
    Call<WeatherData> getWeatherData(@Query("q") String city,
                                     @Query("mode") String mode,
                                     @Query("units") String units,
                                     @Query("APPID") String appId);

}
